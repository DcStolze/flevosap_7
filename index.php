<?php require "header.php";
?>
    <main>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-xs-6">
                    <img src="img/flevosapappel.png">
                </div>


                <div class="col-md-4 col-xs-6" style="margin-top: 44px">
                    <h5 style="color: aqua">Appel Ananas Perzik</h5>
                    <p style="color:green;">Bij Flevosap houden we wel van een<br> feestje. Vele vruchten zijn al in
                        huis. Wie<br> zullen we
                        nog meer uitnodigen? Natuurlijk<br> ananas en perzik! Samen met appel<br> maken we er een
                        fantastisch feest van.</p>
                    <button style="background: aqua">Waar te koop</button>
                    <br><br>
                    <input style="width: 30px">
                    <button style="background: yellow">Toevoegen aan winkelwagen</button>
                </div>

                <div class="col-md-4 col-xs-6" style="margin-top: 44px">
                    <h6 style="color: green">Voedingswaarde per 100 gram</h6>
                    <p style="color: green">Energie 189 KJ(44 kcal)<br> Vetten 0.1 g<br> Waarvan verzadigd 0.0g<br>
                        Koolhydraten: 10.8 g<br>
                        Waarvan suikers 10.3 g<br> Eiwitten 0.3 g<br> Zout 0.004 g</p>
                </div>
                <div class="col-md-4 col-xs-6">
                    <img style="margin-left: 40px" src="img/appel-flevospa.png">
                </div>


                <div class="col-md-4 col-xs-6" style="margin-top: 44px">
                    <h5 style="color: aqua">Appel Ananas Perzik</h5>
                    <p style="color:green;">De meest gegeten appel van Nederland is<br> de Elstar. Uit de hand vinden
                        wij die ook<br> het lekkers, maar wij hebben gemerkt dat<br> als je er ook Jonagold en nog een
                        paar verrassende appelsoorten bij het sap<br> doet, het sap een frisse en nog<br> lekkerdere
                        smaak krijgt. Gellukig groeien<br> deze appels ook in de boomgaarden in de<br> Flevopolder</p>
                    <button style="background: aqua">Waar te koop</button>
                    <br><br>
                    <input style="width: 30px">
                    <button style="background: yellow">Toevoegen aan winkelwagen</button>
                </div>

                <div class="col-md-4 col-xs-6" style="margin-top: 44px">
                    <h6 style="color: green">Voedingswaarde per 100 gram</h6>
                    <p style="color: green">Energie 189 KJ(44 kcal)<br> Vetten 0.1 g<br> Waarvan verzadigd 0.0g<br>
                        Koolhydraten: 10.8 g<br>
                        Waarvan suikers 10.3 g<br> Eiwitten 0.3 g<br> Zout 0.005 g</p>
                </div>
            </div>
        </div>
    </main>

<?php require "footer.php";